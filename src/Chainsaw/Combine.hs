
-- | Machinery to combine different instruction types into one.
module Chainsaw.Combine
    ( -- * Various ways to construct complex changes
      Many (..)
    , Or
    ) where

import Control.Arrow
import GHC.Generics

import Chainsaw.Apply
import Chainsaw.Util

-- | Useful synonym
type Or = Either

-- | To combine changes using `(,)`.
instance
    ( Applies one     m oneRes
    , Applies another m anotherRes
    , Monad           m
    )
  =>
    Applies (one, another) m (oneRes, anotherRes)
  where
    apply = runKleisli $ Kleisli apply *** Kleisli apply
    undo  = runKleisli $ Kleisli undo  *** Kleisli undo

-- | To combine changes using `Either`.
instance
    ( Applies one     m res
    , Applies another m res
    )
  =>
    Applies (Either one another) m res
  where
    apply = either apply apply
    undo  = either undo  undo

-- | Wrapper for foldable "boxes" - so GHC can distinguish instances.
newtype Many f xs = Many { getMany :: f xs }
    deriving (Eq, Ord, Show, Generic)

-- | The Kleene star of change can be applied, too.
instance
    ( Foldable boxOf
    , Applies  change m res
    , Monoid   res
    , Monad    m
    )
  =>
    Applies (Many boxOf change) m res
  where
    apply = foldMapM apply . getMany
    undo  = foldMapM undo  . getMany
