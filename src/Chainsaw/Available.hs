
-- | On-chain resource management.
module Chainsaw.Available
    ( -- * On-chain data access interface
      Available(..)

      -- * Derived data access methods
    , modify
    , require
    , create

      -- * Data access exceptions
    , CannotFind(..)
    , AlreadyExists(..)
    ) where

import Control.Monad.Catch
import Control.Monad.Trans.Class
import Data.Typeable
import GHC.Generics

-- | Ability to access on-chain resource (or part of the chain state).
class Monad m => Available res id m where
    get :: id -> m (Maybe res)  -- Read access.
    put :: id -> res -> m ()    -- Write access.

instance (Monad (t m), Available res id m, MonadTrans t) => Available res id (t m) where
    get id     = lift $ get id
    put id res = lift $ put id res

-- | Exception to be thrown when resource with given id cannot be found
data CannotFind id = CannotFind id
    deriving (Eq, Ord, Show, Typeable, Generic)

instance (Show id, Typeable id) => Exception (CannotFind id)

-- | Modify resource with given id. Requires it to be present.
modify
    ::  ( MonadThrow m
        , Available res id m
        , Exception (CannotFind id)
        )
    =>  id
    ->  (res -> res)
    ->  m ()
modify id f = do
    require id
        >>= put id . f

-- | Get or die.
require
    ::  ( MonadThrow m
        , Available res id m
        , Exception (CannotFind id)
        )
    =>  id
    ->  m res
require id = do
    get id
        >>= maybe (throwM (CannotFind id)) pure

-- | Exception to be thrown if id for new resource is already in use.
data AlreadyExists id = AlreadyExists id
    deriving (Eq, Ord, Show, Typeable, Generic)

instance (Show id, Typeable id) => Exception (AlreadyExists id)

-- | Create resource at given id. Requires it to be NOT present.
create
    ::  forall res id m
    .   ( MonadThrow m
        , Available res id m
        , Exception (AlreadyExists id)
        )
    =>  id
    ->  res
    ->  m ()
create id res = do
    get id >>= \case
        Nothing         -> put id res
        Just (_ :: res) -> throwM $ AlreadyExists id
