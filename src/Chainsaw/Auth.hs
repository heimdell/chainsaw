
-- | Authentication capabilities.
module Chainsaw.Auth
    ( -- * Authentication wrappers
      Authenticated(..)
    , DoneBy(..)
    ) where

import Control.Exception hiding (catch)
import Control.Monad.Catch
import Control.Monad.Reader
import Data.Typeable
import GHC.Generics

import Chainsaw.Apply
import Chainsaw.Combine
import Chainsaw.Util

-- | Makes any 'change' aware of its author.
-- 
--   For it to be applicable in some 'm',
--   requires 'action' to be applicable in `ReaderT identity m`.
--   
--   Will feed the reader context with 'identity'.
--
data Authenticated identity action = Authenticated
    { identity :: identity  -- ^ The author or the source of the commit.
    , action   :: action    -- ^ Action to be modified into authenticated one.
    }
    deriving (Eq, Ord, Show, Generic)

-- | Add an 'identity' parameter to `apply` of 'change'.
instance
    ( MonadCatch m
    , Show       identity
    , Typeable   identity
    , Applies    change (ReaderT identity m) res
    )
  =>
    Applies (Authenticated identity change) m res
  where
    apply (Authenticated person change) =
        runReaderT (apply change) person
            `catch` \e ->
                throwM (DoneBy person (e :: SomeException))

    undo (Authenticated person change) =
        runReaderT (undo change) person
            `catch` \e ->
                throwM (DoneBy person (e :: SomeException))

-- | If each of two changes can be authenticated, their sum (`Either`) can be, too.
instance {-# OVERLAPPING #-}
    ( MonadCatch m
    , Show       identity
    , Typeable   identity
    , Applies    (Authenticated identity one)     m res
    , Applies    (Authenticated identity another) m res
    )
  =>
    Applies (Authenticated identity (Either one another)) m res
  where
    apply (Authenticated person change) = either (apply . Authenticated person) (apply . Authenticated person) change
    undo  (Authenticated person change) = either (undo  . Authenticated person) (undo  . Authenticated person) change

-- | If each of two changes can be authenticated, their product (`(,)`) can be, too.
instance {-# OVERLAPPING #-}
    ( MonadCatch m
    , Show       identity
    , Typeable   identity
    , Applies    (Authenticated identity one)     m oneRes
    , Applies    (Authenticated identity another) m anotherRes
    )
  =>
    Applies (Authenticated identity (one, another)) m (oneRes, anotherRes)
  where
    apply (Authenticated person (one, another)) = do
      oneRes     <- apply (Authenticated person one)
      anotherRes <- apply (Authenticated person another)
      return (oneRes, anotherRes)

    undo (Authenticated person (one, another)) = do
      oneRes     <- undo (Authenticated person one)
      anotherRes <- undo (Authenticated person another)
      return (oneRes, anotherRes)

-- | If each of two changes can be authenticated, their Kleene star/plus (`Foldable`) can be, too.
instance
    ( MonadCatch m
    , Foldable   boxOf
    , Monoid     res
    , Show       identity
    , Typeable   identity
    , Applies    (Authenticated identity change)  m res
    )
  =>
    Applies (Authenticated identity (Many boxOf change)) m res
  where
    apply (Authenticated person changes) = foldMapM (apply . Authenticated person) (getMany changes)
    undo  (Authenticated person changes) = foldMapM (undo  . Authenticated person) (getMany changes)

-- | Is used to wrap exceptions from authenticated contexts,
--   so author of these exceptions can be determined.
data DoneBy identity e = DoneBy identity e
    deriving (Eq, Ord, Show, Typeable, Generic)

instance
    ( Show      identity
    , Typeable  identity
    , Exception e
    )
  =>
    Exception (DoneBy identity e)
