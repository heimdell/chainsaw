
-- | Main interface for applying things to the state.
module Chainsaw.Apply
    ( -- * Interface for change to apply
      Applies(..)
    , Transact(..)
    ) where

import Control.Monad.Catch
import GHC.Generics

-- | Capability of applying some 'change' in the monad 'm' with result 'res'.
--
--   If 'm' has a 'MonadCatch' instance, it must perform rollback on `catch`.
--
class Applies change m res where
    -- | Perform given 'change'.
    apply :: change -> m res
    undo  :: change -> m res

-- | Designate requirement to rollback on failure.
data Transact change = Transact change
    deriving (Eq, Ord, Show, Generic)

-- | `MonadCatch m` must perform rollback on `catch`.
instance (MonadCatch m, Applies change m res) => Applies (Transact change) m res where
    apply (Transact change) = do
        apply change `catch` \(e :: SomeException) ->
            throwM e

    undo (Transact change) = do
        undo change `catch` \(e :: SomeException) ->
            throwM e