
-- | Functions that I was too lazy to import from anywhere.
module Chainsaw.Util
    ( -- * Auxillary functions
      orThrow
    , foldMapM
    ) where

import Control.Monad (unless)
import Control.Monad.Catch (throwM)

-- | Unless condition, throw exception.
infix 1 `orThrow`
bool `orThrow` msg = do
    unless bool $ throwM msg

-- | The `foldMap` inside a `Monad`.
foldMapM :: forall a f m res. (Foldable f, Monad m, Monoid res) => (a -> m res) -> f a -> m res
foldMapM action = foldr append (pure mempty)
    where
    append :: a -> m res -> m res
    append a box = do
        res <- action a
        mappend res <$> box
    