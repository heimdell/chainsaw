
module Chainsaw.Separately
    ( -- * Applying into another monad
      type (~>)
    , Separately(..)
    )
    where

import Chainsaw.Apply

-- | Natural transformation.
type (~>) f g = forall x. f x -> g x

-- | Allows to apply a 'change' over a natural transform.
data Separately n m change = Separately (n ~> m) change

-- | You can delegate applying to another monad (testing, mempool).
instance
    Applies change n res
  =>
    Applies (Separately n m change) m res
  where
    apply (Separately hoist change) = hoist $ apply change
    undo  (Separately hoist change) = hoist $ undo  change
