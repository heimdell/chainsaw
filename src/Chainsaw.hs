
{-|
  Usage:

  First, you declare basic actions:

  > data Transfer = ...
  >
  > instance Applies Transfer EndM Result where
  >     apply = ...
  >     undo  = ...

  (The `apply` method should perform validation and changes at the same time)
  
  /Notice:/ Monad `EndM` may be different from `M`. For instance,
   combinator `Auth identity` to `apply` in `M` requires its wrappee
   to `apply` in `ReaderT identity M`.

  If your basic action requires some on-chain resource, you declare:

  > instance Available Resource Key M where ...

  This, from methods `get` and `put` you provide, allows you to `create`,
  `require` to be present and `modify` specified resource.

  Then you construct a transaction type of provided primitive transformers:

  > type Genesis = Many [] Transfer
  >
  > type Action  = Transfer `Or` SomethingElse `Or` Genesis
  >
  > type Tx      = (Auth Action, PayFees)
  >
  > type Block   = (Many [] Tx, Many Maybe UpdateConsenus)

  Provided you declared `Applies` for all end actions, this will autogenerate
  `instance Applies Block M FullResult`.

  Then you can `apply` and `undo` any transaction of type `Tx`.

  If you need to run tx in some other monad `N`, you can do it with `Separately M N Tx`.
  The value of `Separately` requires a natural transformation from `N` to `M`.

  Oh, and the monad `M` should provide state rollback on `catch`.

  The `Auth identity` wrapper will also wrap all exceptions in `DoneBy identity`
-}

module Chainsaw
    ( module Chainsaw.Apply
    , module Chainsaw.Available
    , module Chainsaw.Auth
    , module Chainsaw.Combine
    , module Chainsaw.Separately
    , module Chainsaw.Util
    )
  where

import Chainsaw.Available
import Chainsaw.Auth
import Chainsaw.Apply
import Chainsaw.Combine
import Chainsaw.Separately
import Chainsaw.Util
