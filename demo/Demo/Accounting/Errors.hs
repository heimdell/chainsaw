
module Demo.Accounting.Errors where

import Control.Exception
import Data.Typeable

import Demo.Accounting.Account

data NegativeAmount = NegativeAmount            deriving (Show, Typeable)
data Overdraft      = Overdraft  Integer Amount deriving (Show, Typeable)
data WrongNonce     = WrongNonce Nonce   Nonce  deriving (Show, Typeable)

instance Exception WrongNonce
instance Exception Overdraft
instance Exception NegativeAmount
