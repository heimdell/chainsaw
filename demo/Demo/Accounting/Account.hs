
module Demo.Accounting.Account where

import GHC.Generics

type Identity = Integer

type Address = Integer

data Account = Account
    { balance :: Integer
    , nonce   :: Nonce
    }
    deriving (Show, Generic)

type Amount = Integer
type Nonce  = Integer

getAddress :: Identity -> Address
getAddress = id

modifyBalance :: (Integer -> Integer) -> Account -> Account
modifyBalance f acc = acc { balance = f (balance acc) }

incrementNonce :: Account -> Account
incrementNonce acc = acc { nonce = nonce acc + 1 }
