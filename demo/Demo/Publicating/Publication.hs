
module Demo.Publicating.Publication where

import Control.Monad.Catch
import GHC.Generics

import Chainsaw

import Demo.Accounting

data Publication payload = Publication
    { payload :: payload
    }
    deriving (Show, Generic)

data PublicationChain payload = PublicationChain
    { item :: Publication payload
    , prev :: Maybe Address
    }
    deriving (Show, Generic)

newtype LastPublication = LastPublication
    { getLastPublication :: Address }
    deriving (Show, Generic)

publish
    ::  ( MonadThrow m
        , Available  (PublicationChain payload) Address  m
        , Available   LastPublication           Identity m
        )
    =>  Identity
    ->  Address
    ->  Publication payload
    ->  m ()
publish who addr pub = do
    maybePrev <- (getLastPublication <$>) <$> get who

    create addr (PublicationChain pub maybePrev)
    put    who  (LastPublication addr)
