
module Demo.Monad where

import Codec.Serialise
import Control.Concurrent.STM
import Control.Monad.Catch
import Control.Monad.Reader
import Control.Monad.State as State
import Control.Monad.IO.Class
import qualified Data.Tree.AVL as AVL
import Data.Hashable
import Data.Default
import Data.ByteString.Lazy (fromStrict, toStrict)
import Data.ByteString (ByteString)
import Data.Typeable
import Database.RocksDB as Rocks
import GHC.Generics
import Unsafe.Coerce
import Data.Word
import Data.Foldable
import qualified Data.Set as Set

newtype AppM a = AppM { runAppM :: ReaderT AppCtx IO a }
    deriving (Functor, Applicative, Monad, MonadReader AppCtx, MonadIO, MonadThrow, MonadCatch)

data AppCtx = AppCtx
    { rocks   :: Rocks.DB
    , prefabs :: TVar (Set.Set AVL.Revision)
    }

hoistAppM :: AppM a -> IO a
hoistAppM (AppM action) = do
    var <- newTVarIO Set.empty
    bracket (open "db" def) close (runReaderT action . (`AppCtx` var))

newtype TxT h k v m a = TxT { runTxT :: StateT (AVL.Map h k v) m a }
    deriving (Functor, Applicative, Monad, MonadIO, MonadThrow, MonadCatch, MonadState (AVL.Map h k v))

deriving instance MonadReader r m => MonadReader r (TxT h k v m)

newtype Mempool m a = Mempool { getMempool :: m a }
    deriving (Functor, Applicative, Monad, MonadIO)

instance MonadTrans Mempool where
    lift = Mempool

class Commit m where
    commit :: m ()

instance (Ord h, Show h, Typeable h, Ord k, Show k, Typeable k, Show v, Typeable v, AVL.Hash h k v, Serialise h, Serialise k, Serialise v) => Commit (TxT h k v AppM) where
    commit = AVL.append @h =<< State.get

instance Commit (Mempool (TxT h k v AppM)) where
    commit = return ()

instance Serialise k => Serialise (AVL.WithBounds k)
instance Serialise AVL.Tilt
instance (Serialise h, Serialise k, Serialise v) => Serialise (AVL.MapLayer h k v h)
        
instance (Serialise h, Show h, Typeable h, Serialise k, Serialise v) => AVL.KVRetrieve h (AVL.MapLayer h k v h) (TxT h k v AppM) where
    retrieve hash = do
        rocks <- asks rocks
        maybe (throwM $ AVL.NotFound hash) pure
            =<< (deserialise' <$>) <$> Rocks.get rocks def (serialise' hash)

instance (Serialise h, Serialise k, Serialise v) => AVL.KVStore h (AVL.MapLayer h k v h) (TxT h k v AppM) where
    massStore kvs = do
        rocks <- asks rocks
        for_ kvs $ \(k, v) -> do
            Rocks.put rocks def (serialise' k) (serialise' v)

instance (Serialise h, Show h, Typeable h, Serialise k, Serialise v) => AVL.KVMutate h (AVL.MapLayer h k v h) (TxT h k v AppM) where
    getRoot = do
        rocks <- asks rocks
        maybe (throwM $ AVL.NoRootExists) (pure . deserialise')
            =<< Rocks.get rocks def (serialise' "root")

    setRoot root = do
        rocks <- asks rocks
        Rocks.put rocks def (serialise' "root") (serialise' root)

    erase hash = do
        rocks <- asks rocks
        Rocks.delete rocks def (serialise' hash)

serialise' :: Serialise a => a -> ByteString
serialise' = toStrict . serialise

deserialise' :: Serialise a => ByteString -> a
deserialise' = deserialise . fromStrict

consumeProofs :: Set.Set AVL.Revision -> TxT h k v AppM ()
consumeProofs set = do
    var <- asks prefabs
    liftIO $ atomically $ modifyTVar' var (<> set)

swapProofs :: AppM (Set.Set AVL.Revision)
swapProofs = do
    var <- asks prefabs
    liftIO $ atomically $ swapTVar var Set.empty