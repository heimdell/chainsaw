
module Demo.Instr.Publicate where

import Control.Monad.Catch
import Control.Monad.Reader

import Data.Map as Map

import Chainsaw

import Demo.Accounting
import Demo.Publicating
import Demo.Proof

data Instr payload = Publish Address payload

instance
    ( MonadThrow m
    , Available  (PublicationChain payload) Address  m
    , Available   LastPublication           Identity m
    )
  =>
    Applies (Instr payload) (ReaderT Identity m) Proof
  where
    apply (Publish addr payload) = do
        self <- ask
        publish self addr (Publication payload)
        return $ Proof ()
