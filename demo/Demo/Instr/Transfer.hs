
module Demo.Instr.Transfer where

import Control.Monad.Catch
import Control.Monad.Reader

import Chainsaw

import Demo.Accounting
import Demo.Proof

data Instr = Transfer Amount Address Nonce

instance (MonadThrow m, Available Account Address m) => Applies Instr (ReaderT Identity m) Proof where
    apply (Transfer amount to theNonce) = do

        from    <- getAddress <$> ask
        fromAcc <- require from

        amount                   >= 0        `orThrow` NegativeAmount
        balance fromAcc - amount >= 0        `orThrow` Overdraft  (balance fromAcc) amount
        nonce   fromAcc          == theNonce `orThrow` WrongNonce (nonce   fromAcc) theNonce

        modify from (modifyBalance (subtract amount) . incrementNonce)
        modify to   (modifyBalance (+        amount))

        return $ Proof ()
