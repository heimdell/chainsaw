
module Demo.Instr (Instr) where

import Chainsaw
import Demo.Accounting

import qualified Demo.Instr.Transfer as Transfer
import qualified Demo.Instr.Publicate as Publicate
        
type Instr =
    Authenticated Identity
        (Either
            (Transfer.Instr)
            (Publicate.Instr ()))
