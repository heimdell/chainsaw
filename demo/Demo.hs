
module Demo
    ( module Demo.Instr
    , module Demo.Proof
    , module Chainsaw
    ) where

import Codec.Serialise
import Control.Monad.State as State
import qualified Data.Tree.AVL as AVL
import Data.Hashable
import GHC.Generics

import Chainsaw

import Demo.Accounting hiding (Instr)
import Demo.Publicating hiding (Instr)
import Demo.Instr (Instr)
import Demo.Proof
import Demo.Instr.Transfer hiding (Instr)
import Demo.Monad

type Hash = Int

data Key
    = AccountAddress Address
    | PubChainAddress Address
    | LastPubId Identity
    deriving (Eq, Ord, Show, Generic)

data Value
    = TheAccount   Account
    | ThePubChain (PublicationChain ())
    | TheLastPub   LastPublication
    deriving (Show, Generic)

instance Serialise Account
instance Serialise a => Serialise (Publication a)
instance Serialise a => Serialise (PublicationChain a)
instance Serialise Key
instance Serialise LastPublication
instance Serialise Value

-- Make hash a Monoid in AVL!s
instance AVL.Hash Hash Key Value where
    hashOf = \case
        AVL.MLBranch r _ m c t l r' ->
            foldl1 combine [hash r, hash m, hash c, hash t, l, r']
          where
            combine a b = hash (a + b)

instance Hashable Key
instance Hashable AVL.Tilt
instance Hashable key => Hashable (AVL.WithBounds key)

instance Available Account Address (TxT Hash Key Value AppM) where
    get addr = do
        tree <- State.get
        ((res, proof), tree1) <- AVL.lookup (AccountAddress addr) tree
        State.put tree
        consumeProofs proof
        return $ (\(TheAccount acc) -> acc) <$> res

instance Available (PublicationChain ()) Address  (TxT Hash Key Value AppM)
instance Available  LastPublication      Identity (TxT Hash Key Value AppM)
